package com.university.springuniversitydemo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="vagetation_period")
public class VegetationPeriod {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "information")
	private String information;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="fruit_tree_id")
	@JsonManagedReference	
	private FruitTree fruitTree;

	public VegetationPeriod() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public FruitTree getFruitTree() {
		return fruitTree;
	}

	public void setFruitTree(FruitTree fruitTree) {
		this.fruitTree = fruitTree;
	}
	

}
