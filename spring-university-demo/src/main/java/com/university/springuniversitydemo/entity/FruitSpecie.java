package com.university.springuniversitydemo.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="fruitspecie")
public class FruitSpecie {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name = "name")
	private String name;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="fruit_tree_id")
	@JsonManagedReference	
	private FruitTree fruitTree;
//	@OneToMany(fetch=FetchType.LAZY,
//			mappedBy="fruitspecie",
//			cascade=CascadeType.ALL)
//	@JsonManagedReference	
//	private List<Variety> varieties;
//	
//	@OneToMany(fetch=FetchType.LAZY,
//			mappedBy="fruitspecie",
//			cascade=CascadeType.ALL)
//	@JsonManagedReference	
//	private List<Rootstock> rootStocks;

	public FruitSpecie() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FruitTree getFruitTree() {
		return fruitTree;
	}

	public void setFruitTree(FruitTree fruitTree) {
		this.fruitTree = fruitTree;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

//	public List<Rootstock> getRootStocks() {
//		return rootStocks;
//	}
//
//	public void setRootStocks(List<Rootstock> rootStocks) {
//		this.rootStocks = rootStocks;
//	}
//	
//	public List<Variety> getVarieties() {
//		return varieties;
//	}
//
//	public void setVarieties(List<Variety> varieties) {
//		this.varieties = varieties;
//	}

}
