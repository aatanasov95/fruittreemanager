package com.university.springuniversitydemo.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="field")
public class Field {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name = "name")
	private String name;
	
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, 
			CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="owner_id")
	@JsonBackReference
	private Owner owner;
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, 
			CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="agronomist_id")
	@JsonBackReference
	private Agronomist agronomist;
	@OneToMany(fetch=FetchType.LAZY,
			mappedBy="field",
			cascade=CascadeType.ALL)
	@JsonManagedReference	
	private List<Orchard> orchards;
	
	public Field() {
	}
	
	public Field(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Owner getOwner() {
		return owner;
	}
	
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	
	public Agronomist getAgronomist() {
		return agronomist;
	}
	
	public void setAgronomist(Agronomist agronomist) {
		this.agronomist = agronomist;
	}
	
	public List<Orchard> getOrchards() {
		return orchards;
	}
	
	public void setOrchards(List<Orchard> orchards) {
		this.orchards = orchards;
	}
	
}
