package com.university.springuniversitydemo.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="row")
public class Row {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="distance_to_next_row", precision = 5, scale = 4)
	@Type(type = "big_decimal")
	private double distanceToNextRow;
	@Column(name="distance_to_first_tree", precision = 5, scale = 4)
	@Type(type = "big_decimal")
	private double distanceToFirstTree;

	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, 
			CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="orchard_id")
	@JsonBackReference
	private Orchard orchard;
	@OneToMany(fetch=FetchType.LAZY,
			mappedBy="row",
			cascade=CascadeType.ALL)
	@JsonManagedReference	
	private List<FruitTree> fruitTrees;
	
	public Row() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getDistanceToNextRow() {
		return distanceToNextRow;
	}

	public void setDistanceToNextRow(double distanceToNextRow) {
		this.distanceToNextRow = distanceToNextRow;
	}

	public double getDistanceToFirstTree() {
		return distanceToFirstTree;
	}

	public void setDistanceToFirstTree(double distanceToFirstTree) {
		this.distanceToFirstTree = distanceToFirstTree;
	}

	public Orchard getOrchard() {
		return orchard;
	}

	public void setOrchard(Orchard orchard) {
		this.orchard = orchard;
	}

	public List<FruitTree> getFruitTrees() {
		return fruitTrees;
	}

	public void setFruitTrees(List<FruitTree> fruitTrees) {
		this.fruitTrees = fruitTrees;
	}
	
}
