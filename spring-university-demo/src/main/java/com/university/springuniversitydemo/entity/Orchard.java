package com.university.springuniversitydemo.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="orchard")
public class Orchard {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name="default_distance_to_next_row", precision = 5, scale = 4)
	@Type(type = "big_decimal")
	private double distanceToNextRow;
	@Column(name="default_distance_to_next_tree", precision = 5, scale = 4)
	@Type(type = "big_decimal")
	private double distanceToNextTree;
	@Column(name="plant_date")
	private Date plantDate;
	

	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, 
			CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="field_id")
	@JsonBackReference
	private Field field;
	@ManyToMany(fetch=FetchType.LAZY, 
			cascade= {CascadeType.DETACH, CascadeType.MERGE, 
			CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(
			name="agronomist_orchard",
			joinColumns = @JoinColumn(name="orchard_id"),
			inverseJoinColumns = @JoinColumn(name="agronomist_id")
			)
	@JsonManagedReference
	private List<Agronomist> agronomists;
	@OneToMany(fetch=FetchType.LAZY,
			mappedBy="orchard",
			cascade=CascadeType.ALL)
	@JsonManagedReference	
	private List<Row> rows;
	
	
	public Orchard() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getDistanceToNextRow() {
		return distanceToNextRow;
	}
	public void setDistanceToNextRow(double distanceToNextRow) {
		this.distanceToNextRow = distanceToNextRow;
	}
	public double getDistanceToNextTree() {
		return distanceToNextTree;
	}
	public void setDistanceToNextTree(double distanceToNextTree) {
		this.distanceToNextTree = distanceToNextTree;
	}
	public Date getPlantDate() {
		return plantDate;
	}
	public void setPlantDate(Date plantDate) {
		this.plantDate = plantDate;
	}
	public Field getField() {
		return field;
	}
	public void setField(Field field) {
		this.field = field;
	}
	public List<Agronomist> getAgronomists() {
		return agronomists;
	}
	public void setAgronomists(List<Agronomist> agronomists) {
		this.agronomists = agronomists;
	}
	public List<Row> getRows() {
		return rows;
	}
	public void setRows(List<Row> rows) {
		this.rows = rows;
	}
	
}
