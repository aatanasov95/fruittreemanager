package com.university.springuniversitydemo.entity;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="fruit_tree")
public class FruitTree {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="plant_date")
	private Date plantDate;
	@Column(name="distance_to_next_tree", precision = 5, scale = 4)
	@Type(type = "big_decimal")
	private double distanceToNextTree;
	@Column(name="distance_against_first_row", precision = 5, scale = 4)
	@Type(type = "big_decimal")
	private double distanceAgainstFirstRow;
	
	
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, 
			CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name = "row_id")
	@JsonBackReference
	private Row row;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="fruitspecie_id")
	@JsonManagedReference
	private FruitSpecie fruitSpecie;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="variety_id")
	@JsonManagedReference
	private Variety variety;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="rootstock_id")
	@JsonManagedReference
	private Rootstock rootstock;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="vegetation_period_id")
	@JsonManagedReference
	private VegetationPeriod vegetationPeriod;
	
	public FruitTree() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getPlantDate() {
		return plantDate;
	}

	public void setPlantDate(Date plantDate) {
		this.plantDate = plantDate;
	}

	public double getDistanceToNextTree() {
		return distanceToNextTree;
	}

	public void setDistanceToNextTree(double distanceToNextTree) {
		this.distanceToNextTree = distanceToNextTree;
	}

	public double getDistanceAgainstFirstRow() {
		return distanceAgainstFirstRow;
	}

	public void setDistanceAgainstFirstRow(double distanceAgainstFirstRow) {
		this.distanceAgainstFirstRow = distanceAgainstFirstRow;
	}

	public Row getRow() {
		return row;
	}

	public void setRow(Row row) {
		this.row = row;
	}

	public FruitSpecie getFruitSpecie() {
		return fruitSpecie;
	}

	public void setFruitSpecie(FruitSpecie fruitSpecie) {
		this.fruitSpecie = fruitSpecie;
	}

	public Variety getVariety() {
		return variety;
	}

	public void setVariety(Variety variety) {
		this.variety = variety;
	}

	public Rootstock getRootstock() {
		return rootstock;
	}

	public void setRootstock(Rootstock rootstock) {
		this.rootstock = rootstock;
	}

	public VegetationPeriod getVegetationPeriod() {
		return vegetationPeriod;
	}

	public void setVegetationPeriod(VegetationPeriod vegetationPeriod) {
		this.vegetationPeriod = vegetationPeriod;
	}
	
}
