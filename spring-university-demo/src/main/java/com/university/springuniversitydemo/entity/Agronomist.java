package com.university.springuniversitydemo.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name="agronomist")
public class Agronomist {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@OneToMany(fetch=FetchType.LAZY,
			mappedBy="agronomist",
			cascade=CascadeType.ALL)
	@JsonManagedReference
	private List<Feedback> feedbacks;
	@ManyToMany(fetch=FetchType.LAZY, 
			cascade= {CascadeType.DETACH, CascadeType.MERGE, 
			CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(
			name="owner_agronomist",
			joinColumns = @JoinColumn(name="agronomist_id"),
			inverseJoinColumns = @JoinColumn(name="owner_id")
			)
	@JsonManagedReference
	private List<Owner> owners;
	@ManyToMany(fetch=FetchType.LAZY, 
			cascade= {CascadeType.DETACH, CascadeType.MERGE, 
			CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(
			name="agronomist_orchard",
			joinColumns = @JoinColumn(name="agronomist_id"),
			inverseJoinColumns = @JoinColumn(name="orchard_id")
			)
	@JsonManagedReference
	private List<Orchard> orchards;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="user_id")
	@JsonManagedReference
	private User user;
	
	public Agronomist() {
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public List<Feedback> getFeedbacks() {
		return feedbacks;
	}

	public void setFeedbacks(List<Feedback> feedbacks) {
		this.feedbacks = feedbacks;
	}

	public List<Owner> getOwners() {
		return owners;
	}

	public void setOwners(List<Owner> owners) {
		this.owners = owners;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Orchard> getOrchards() {
		return orchards;
	}

	public void setOrchards(List<Orchard> orchards) {
		this.orchards = orchards;
	}
	
}
