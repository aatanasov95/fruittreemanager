package com.university.springuniversitydemo.service;


import org.springframework.security.core.userdetails.UserDetailsService;

import com.university.springuniversitydemo.user.CrmUser;

import asdzxca.User;

public interface UserService extends UserDetailsService {

    User findByUserName(String userName);
    void save(CrmUser crmUser);
}
