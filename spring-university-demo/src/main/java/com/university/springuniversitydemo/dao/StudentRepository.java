package com.university.springuniversitydemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import asdzxca.Student;
import asdzxca.User;

public interface StudentRepository extends JpaRepository<Student, Integer> {
	Student findByUser(User user);
	
}
