package com.university.springuniversitydemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import asdzxca.University;
@RepositoryRestResource(path="university")
public interface UniversityRepository extends JpaRepository<University, Integer> {

}
