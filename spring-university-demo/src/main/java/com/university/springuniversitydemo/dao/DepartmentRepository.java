package com.university.springuniversitydemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import asdzxca.Department;

public interface DepartmentRepository extends JpaRepository<Department, Integer> {

}
