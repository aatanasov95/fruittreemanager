package com.university.springuniversitydemo.dao;

import asdzxca.Role;

public interface RoleDao {

	public Role findRoleByName(String theRoleName);
	
}
